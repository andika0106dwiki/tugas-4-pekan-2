<?php
class Animal
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function get_legs($legs)
    {
        return $legs;
    }

    public function get_cold_blooded($cold_blooded)
    {
        return $cold_blooded;
    }
}
