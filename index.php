<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>

<body>
    <?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal('Shaun');
    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->get_legs(4) . "<br>";
    echo "cold blooded : " . $sheep->get_cold_blooded('no') . "<br> <br>";

    $frog = new Frog('buduk');
    echo "Name : " . $frog->name . "<br>";
    echo "legs : " . $frog->get_legs(4) . "<br>";
    echo "cold blooded : " . $frog->get_cold_blooded('no') . "<br>";
    echo "Yell : " . $frog->jump() . "<br><br>";

    $kera = new Ape('kera sakti');
    echo "Name : " . $kera->name . "<br>";
    echo "legs : " . $kera->get_legs(2) . "<br>";
    echo "cold blooded : " . $kera->get_cold_blooded('no') . "<br>";
    echo "Yell : " . $kera->yell() . "<br><br>";

    ?>
</body>

</html>